package casir.matchgame;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.Exception;

class MatchGameTest {

    @BeforeEach // this function is called before each test
    void setUp(){
        // init context
    }

    @Test // this is a test
    void addTwoInt() throws Exception {
        int c = MatchGame.add(2, 1);
        assertEquals(3, c); // see also assertTrue, assertContains...
    }

    @Test // this test is valid if the expected exception is thrown
    void addSumIs0() {
        Executable sum = () -> {int c = MatchGame.add(-1, 1);};
        assertThrows(Exception.class, sum);
    }
}
